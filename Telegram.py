import telepot
import threading
import time

import Message

class Telegram(threading.Thread):
    def __init__(self, moana, sleepTime):
        threading.Thread.__init__(self)
        print("Create Telegram")
        self.bot = None
        
        self.moana = moana
        
        self.stopFlag = False
        self.chat_id = ""
        
        self.messageBuffer = []

        self.sleepTime = sleepTime
        
    def connect(self, token):
        self.bot = telepot.Bot(token)
        self.bot.message_loop(self.requestHandle)
    
    def stop(self):
        print("Stop Telegram")
        self.stopFlag = True
        
    def sendMessage(self, message):
        msg = """
        [ {0} | {1} ]
        {2}
        """.format(message.serverName, message.time, message.content)
        
        self.bot.sendMessage(self.chat_id, msg)
        
    def requestHandle(self, msg):
        if self.chat_id == "":
            self.chat_id = msg['chat']['id']
        command = msg['text']
        print("User command: {0}".format(command))

        reply = self.moana.handleTelegramRequest(command)    
        self.sendMessage(reply)
    
    def setMessage(self, message):
        if self.chat_id == "":
            print("Chat_id is empty")
            return
        msg = Message.Message()
        msg.setMessage(message.serverName, message.content)
        
        self.messageBuffer.append(msg)
                
    def run(self):
        try:
            while True:
                if self.stopFlag:
                    break
                
                if self.messageBuffer:
                    for msg in self.messageBuffer:
                        self.sendMessage(msg)
                    del self.messageBuffer[:]
                    
                time.sleep(self.sleepTime)
                
        except KeyboardInterrupt:
            self.moana.stop()
                    
        print("Telegram End")