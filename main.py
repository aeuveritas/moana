#!/usr/bin/env python3

import Moana
import sys

def main(argv):
    moana = Moana.Moana()
    
    ready = moana.initialize(argv)
    
    if ready:
        moana.run()
    
if __name__ == "__main__":
    main(sys.argv[1:])

