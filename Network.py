import threading

import subprocess
import socket
import time

class Network(threading.Thread):
    def __init__(self, moana, nameserver, sleepTime):
        threading.Thread.__init__(self)
        print("Create Network")
        
        self.moana = moana
        
        self.localIp = ""
        
        self.crashed = {}

        self.stopFlag = False
        
        self.nameserver = nameserver
        
        self.sleepTime = sleepTime
        
    def getIpAddress(self):
        if self.localIp == "":
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("8.8.8.8", 80))
            self.localIp = s.getsockname()[0]
        
        return self.localIp
    
    def stop(self):
        print("Stop Network")
        self.stopFlag = True
        
    def down(self):
        msg = ""

        if not self.crashed:
            msg = "All is alive"
        else:
            for name in self.crashed:
                msg ="{0}({1})".format(msg, name)
            msg = "Down server list: {0}".format(msg)

        return msg

    def run(self):
        while True:
            if self.stopFlag:
                break
            
            for name, ip in self.nameserver.items():
                if ip == self.localIp:
                    continue
                
                try:
                    subprocess.check_output(["ping", "-c", "1", ip],
                                                   stderr=subprocess.STDOUT,
                                                   universal_newlines=True)
                    if name in self.crashed:
                        del self.crashed[name]
                        msg = "{0} is returned".format(name)
                        self.moana.report(msg)
                except:
                    if not name in self.crashed:
                        self.crashed[name] = ip
                        msg = "{0} is down".format(name)
                        self.moana.report(msg)
                    
            time.sleep(self.sleepTime)
            
        print("Network End")
