import Telegram
import Network

import yaml
import time
import datetime

import sys
import getopt

import Message

class Moana():
    def __init__(self):
        self.telegram = None
        self.network = None
        
        self.nameserver = {}
        self.name = ""
        self.token = ""
        
        self.configFile = "config.yaml"
        
        self.startFlag = True
        self.stopFlag = False

    def printHelp(self):
        print("""
        moana.py [OPITON] [PARAM]
        [OPTION]
        -h, --help
            : print this help message
        -c, --config
            : configuration file
        """)
        
        
    def parseParam(self, argv):
        try:
            opts, args = getopt.getopt(argv, "hc:", ["help", "config="])
        except getopt.GetoptError:
            self.printHelp()
            return False
            
        ret = True
        for opt, arg in opts:
            if opt in ["-h", "--help"]:
                self.printHelp()
                ret = False
            elif opt in ("-c", "--config"):
                self.configFile = arg
            else:
                self.printHelp()
                ret = False
                
        return ret
        
    def initialize(self, argv):
        if not self.parseParam(argv):
            return False
        
        if not self.readConfig():
            return False
        
        self.telegram = Telegram.Telegram(self, self.sleepTime)
        self.network = Network.Network(self, self.nameserver, self.sleepTime)
            
        self.connect()
        
        return True
        
    def readConfig(self):
        try:
            file = open(self.configFile, "r")
            configs = yaml.load_all(file)
        except Exception as err:
            print(err)
            self.printHelp()
            return False
            
        try:
            for config in configs:
                self.token = config["Configuration"]["token"]
                self.nameserver = config["IP"]
                self.sleepTime = config["Configuration"]["sleepTime"]
        except Exception as err:
            print("Error: {0}".format(err))
        finally:
            file.close()

        return True
        
    def connect(self):
        token = self.token
        
        self.telegram.connect(token)
        
        localIp = self.network.getIpAddress()
        
        for name, ip in self.nameserver.items():
            if localIp == ip:
                self.name = name
            
        self.launch()
        
    def telegramHelp(self):
        msg = """
        /help      : Print this message
        /start     : Start Moana 
        /stop      : Stop Moana
        /terminate : Terminate Moana
        /time      : Get current time
        /down      : Show list of crashed server
        """
        
        return msg
        
    def handleTelegramRequest(self, command):
        msg = ""
        
        if not self.startFlag:
            if command == "/start":
                self.startFlag = True
                msg = "Start"
            elif command == "/help":
                msg = self.telegramHelp()
            else:
                msg = "Moana is stopped"
        else:
            if command == "/time":
                msg = datetime.datetime.now()
            elif command == "/help":
                msg = self.telegramHelp()
            elif command == "/stop":
                self.startFlag = False
                msg = "Stop"
            elif command == "/terminate":
                self.stopFlag = True
                self.terminate()
                msg = "Terminate"
            elif command == "/down":
                msg = self.network.down()
            else:
                msg = "Invalid input"
        
        ret = Message.Message()
        ret.setMessage(name = self.name, content = msg)
        
        return ret
    
    def report(self, msg):
        message = Message.Message()
        message.setMessage(name = self.name, content = msg)
        
        self.telegram.setMessage(message)
        
    def launch(self):
        print("Start Moana")
        self.telegram.start()
        self.network.start()
        
    def terminate(self):
        print("Stop Moana")
        self.telegram.stop()
        self.network.stop()
        
    def run(self):
        try:
            while True:
                if self.stopFlag:
                    break
                time.sleep(self.sleepTime)
        except KeyboardInterrupt:
            self.terminate()
        
        print("Moana End")