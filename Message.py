import datetime

class Message():
    def __init__(self):
        self.serverName = ""
        self.content = ""
        
        self.time = self.getCurrentTime()
        
    def getCurrentTime(self):
        now = datetime.datetime.now()
        
        time = "{0}:{1}:{2}".format(now.hour,
                                    now.minute, 
                                    now.second)
        
        return time
        
    def setMessage(self, name = None, content = None):
        if not name is None:
            self.serverName = name
            
        if not content is None:
            self.content = content
    
